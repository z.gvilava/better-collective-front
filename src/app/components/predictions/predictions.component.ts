import { Component, OnInit } from '@angular/core';
import { PredictionsService } from "../../services/predictions.service";
import { PredictionsInterface } from "../../interfaces/predictions.interface";

@Component({
  selector: 'app-predictions',
  templateUrl: './predictions.component.html',
  styleUrls: ['./predictions.component.scss']
})
export class PredictionsComponent implements OnInit {

  data: Array<PredictionsInterface>;

  constructor(
    private predictionsService: PredictionsService
  ) { }

  getPredictions()
  {
      return this.predictionsService.index()
        .subscribe(
          (res:Array<PredictionsInterface>) => {
          this.data = res;
        }
      )
  }

  ngOnInit() {
    this.getPredictions();
  }

}

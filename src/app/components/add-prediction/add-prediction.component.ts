import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { EventsService } from "../../services/events.service";
import { PredictionsService } from "../../services/predictions.service";
import { EventsInterface } from "../../interfaces/events.interface";
import { Router }     from '@angular/router';

@Component({
  selector: 'app-add-prediction',
  templateUrl: './add-prediction.component.html',
  styleUrls: ['./add-prediction.component.scss']
})
export class AddPredictionComponent implements OnInit {

  data: Array<EventsInterface>;
  marketType = [
    { name: "1x2", title: "Result Prediction" },
    { name: "correct_score", title: "Correct Score"}
  ];
  predictionForm: FormGroup;
  resultPrediction = ['1', '2', 'x'];

  constructor(
    private eventsService: EventsService,
    private predictionsService: PredictionsService,
    private router: Router
  ) { }

  getEvents()
  {
    return this.eventsService.index()
      .subscribe(
        (res:Array<EventsInterface>) => {
          this.data = res;
        }
      )
  }

  initForm(){
    this.predictionForm = new FormGroup({
      'event_id': new FormControl(null, [Validators.required]),
      'market_type': new FormControl(null, [Validators.required]),
      'prediction': new FormControl(null, [Validators.required])
    });
  }

  onSubmit() {
    console.log(this.predictionForm.value);
    if (this.predictionForm.valid){
      return this.predictionsService.store(this.predictionForm.value).subscribe(
        res => {
          if (res.status === 201) {
            return this.router.navigate([''])
          }
        }, err =>{
          console.log(err)
        })
    }else return false;
  }

  onMarketTypeChange()
  {
    this.predictionForm.get('market_type').valueChanges.subscribe(val => {
      this.predictionForm.controls['prediction'].setValidators(null);
      if (val == "correct_score"){
        this.predictionForm.controls['prediction'].setValidators( [ Validators.required, Validators.pattern('^[0-9]+:[0-9]+$') ])
      }else if(val == "1x2"){
        this.predictionForm.controls['prediction'].setValidators( [ Validators.required, Validators.maxLength(1), Validators.pattern('^1|x|2')])
      }
      this.predictionForm.controls['prediction'].updateValueAndValidity();
    })
  }

  ngOnInit() {
    this.getEvents();
    this.initForm();
    this.onMarketTypeChange();
  }

}

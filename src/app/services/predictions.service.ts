import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class PredictionsService {

  constructor(
    private http: HttpClient
  ) {}

  index(): Observable<object>
  {
    return this.http.get(environment.apiUrl + 'predictions');
  }

  store(data){
    return this.http.post(environment.apiUrl + 'predictions', data, {observe: 'response'});
  }

}

export interface PredictionsInterface {

  id: number,
  name: string,
  event_name: string,
  market_type: string,
  prediction: string,
  status: string

}
